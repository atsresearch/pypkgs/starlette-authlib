{
  description = "atsr python libraries";

  inputs = {
    settings = {
      url = "path:./nix/settings.nix";
      flake = false;
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, ... }@inputs:

    flake-utils.lib.eachDefaultSystem (system: let

      settings = import inputs.settings;
      nixpkgs = inputs.nixpkgs.legacyPackages.${system};
      pythonPackages = nixpkgs.${settings.python+"Packages"};

      thispkg = pythonPackages.buildPythonPackage rec {
        pname = "starlette-authlib";
        version = "0.1.8";
        src = pythonPackages.fetchPypi {
          inherit pname version;
          sha256 = "2fcc4a655528bee13a6f198561e0e693e85f8c468d2b7eebddf0b96f456ae020";
        };
        buildInputs = with pythonPackages; [ 
          authlib
          uvicorn
          starlette
        ];
        propagatedBuildInputs = with pythonPackages; [ 
          authlib
          uvicorn
          starlette
        ];
        prePatch = ''
          # remove some annoyingly/unnecessarily tight requirement checks
          sed -i "/'install_requires':.*/d" setup.py
        '';
      };

      pythonPackaged = nixpkgs.${settings.python}.withPackages (p: with p; [ thispkg ]);

    in {

      defaultPackage = thispkg;
        
      devShell = nixpkgs.mkShell {
        buildInputs = [ pythonPackaged ];
        shellHook = ''
          export PYTHONPATH=${pythonPackaged}/${pythonPackaged.sitePackages}
        '';
      };

    });
}
